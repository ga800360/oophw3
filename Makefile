all:
	javac ./src/POOBBS.java
	javac ./src/POOUser.java
	javac ./src/POOBoard.java
	javac ./src/POOArticle.java
	javac ./src/POODirectory.java
run:
	cd ./src
	java POOBBS
clean:
	rm -f ./src/*.class
	rm -f ./src/*.profile
