import java.util.*;
import java.io.*;
import sun.misc.*;
import javax.crypto.*;
import java.security.*;
import javax.crypto.spec.*;
public class POOUser{
	private String userName;
	private String name;
	private byte[] hashByte;
	private int credit;
	private Console console = System.console();
	private Scanner scanner = new Scanner(System.in);
	private POODirectory home;
	private LinkedList<currentAt> usrCur;
	private int usrType;
	
	public POOUser(){
		userName = new String("guest");
		name = new String("guest");
		home = new POODirectory("home");
		usrCur = new LinkedList<currentAt>();
		currentAt curFirst = new currentAt(home);
		usrCur.addFirst(curFirst);
		usrType = 2;
		credit = 0;
	}
	
	private class currentAt{
		public POODirectory dir;
		public POOBoard brd;
		public POOArticle art;
		public currentAt(POODirectory current){
			dir = current;
			brd = null;
			art = null;
		}
		public currentAt(POOBoard current){
			dir = null;
			brd = current;
			art = null;
		}		
		public currentAt(POOArticle current){
			dir = null;
			brd = null;
			art = current;
		}
		public void sendMes(String Mes){
			if(dir!=null) dir.working(Mes);
			else if(brd!=null) brd.working(Mes);
			else if(art!=null) art.working(Mes);
			else{
				return ;
			}
		}
		public int curType(){
			if(dir!=null) return 0;
			else if(brd!=null) return 1;
			else if(art!=null) return 2;
			else{
				return -1;
			}
		}
	}
	
	private void dealInst(String inst){
		try{
			String inst1, destTmp;
			int dest;
			int curType = usrCur.getLast().curType();
			currentAt next;
			StringTokenizer Tok = new StringTokenizer(inst);
			inst1 = (String)Tok.nextElement();
			if(inst1.equals("save")) saveUserSetting();
			else if(inst1.equals("help")) help();
			else{
				if(curType == 0){
					if(inst1.equals("add")) usrCur.getLast().sendMes(inst);
					else if(inst1.equals("del")) usrCur.getLast().sendMes(inst);
					else if(inst1.equals("move")) usrCur.getLast().sendMes(inst);
					else if(inst1.equals("show")) usrCur.getLast().sendMes(inst);
					else if(inst1.equals("cd")){
						destTmp = (String)Tok.nextElement();
						if(destTmp.equals("..")){
							if(usrCur.getLast().dir == home) return ;
							usrCur.removeLast();
							return ;
						}
						dest = Integer.valueOf(destTmp);
						curType = usrCur.getLast().dir.getObjectType(dest);
						if(curType == 0){
							next = new currentAt(usrCur.getLast().dir.getDir(dest));
							usrCur.addLast(next);
						}
						else if(curType == 1){
							next = new currentAt(usrCur.getLast().dir.getBrd(dest));
							usrCur.addLast(next);
						}
						else{
							System.out.println("position error");
						}
					}
					else{	
						System.out.println("No such instruction:( Please use \"help\" to get some help.");
					}
					return ;
				}
				else if(curType == 1){
					if(inst1.equals("add")) usrCur.getLast().sendMes(inst+" "+userName);
					else if(inst1.equals("del")) usrCur.getLast().sendMes(inst);
					else if(inst1.equals("move")) usrCur.getLast().sendMes(inst);
					else if(inst1.equals("show")) usrCur.getLast().sendMes(inst);
					else if(inst1.equals("cd")){
						destTmp = (String)Tok.nextElement();
						if(destTmp.equals("..")){
							usrCur.removeLast();
							return ;
						}
						dest = Integer.valueOf(destTmp);
						next = new currentAt(usrCur.getLast().brd.getArt(dest));
						usrCur.addLast(next);
						return ;
					}
					else{
						System.out.println("No such instruction:( Please use \"help\" to get some help.");
					}
					return ;
				}
				else if(curType == 2){
					if(inst1.equals("push")) usrCur.getLast().sendMes(inst+" "+userName);
					else if(inst1.equals("boo"))  usrCur.getLast().sendMes(inst+" "+userName);
					else if(inst1.equals("arrow"))  usrCur.getLast().sendMes(inst+" "+userName);
					else  if(inst1.equals("show"))  usrCur.getLast().sendMes(inst+" "+userName);
					else if(inst1.equals("list"))  usrCur.getLast().sendMes(inst+" "+userName);
					else if(inst1.equals("cd")){
						destTmp = (String)Tok.nextElement();
						if(destTmp.equals("..")){
							usrCur.removeLast();
							return ;
						}
						else System.out.println("No such instruction:( Please use \"help\" to get some help.");
					}
					else{
						System.out.println("No such instruction:( Please use \"help\" to get some help.");
					}
					return ;
				}
				else{
					System.out.println("No such instruction:( Please use \"help\" to get some help.");
				}
			}
		}
		catch(Exception e){
			System.out.println("No such instruction:( Please use \"help\" to get some help.");
			return ;
		}
		return ;
	}
	
	public String getPath(){
		int i;
		String path = new String("");
		for(i = 0;i<usrCur.size();i++){
			if(usrCur.get(i).curType() == 0) path = new String(path+usrCur.get(i).dir.getName()+"/");
			else if(usrCur.get(i).curType() == 1) path = new String(path+usrCur.get(i).brd.getName()+"/");
			else path = new String(path+usrCur.get(i).art.getTitle());
		}
		return path;
	}
	
	public void getInst(){
		String inst;
		while(true){
			System.out.print(userName+"@POOBBS ["+getPath()+"] ");
			inst = scanner.nextLine();
			dealInst(inst);
		}
	}
	
	private void setName(){
		System.out.print("Please enter your name: ");
		name = scanner.nextLine();
	}
	
	private void help(){
		System.out.println("===========POOBBS by b99902046 �L�z��===========\n");
		System.out.println("======Board and Directory instructions=======");
		System.out.println("save\n");
		System.out.println("save all the user and board settings");
		System.out.println("=============");
		System.out.println("add [-d | -b] [Name]\n");
		System.out.println("\t-d to add a directory\n\t-b ot add a board");
		System.out.println("=============");
		System.out.println("del [Position]\n");
		System.out.println("delete target position object");
		System.out.println("=============");
		System.out.println("move [src] [dest]\n");
		System.out.println("move target from src to dest");
		System.out.println("=============");
		System.out.println("show\n");
		System.out.println("show content");
		System.out.println("=============");
		System.out.println("cd [Position|..]\n");
		System.out.println("cd to target Position or to parent location\n");
		System.out.println("======Article only options=======");
		System.out.println("list\n");
		System.out.println("list the brief information");
		System.out.println("=============");
		System.out.println("[push|boo|arrow] [Evaluation]\n");
		System.out.println("add evaluation through push/boo/arrow");
		System.out.println("================================================\n");
	}

	//the mehtod is the user data system, it will see what your action is
	public int login() throws Exception{
		while(true){
			System.out.println("Please enter your username, enter \"new\" to sign up, \"guest\" to play as guest");
			userName = scanner.nextLine();
			if(userName.equals("guest")) return 2;
			else if(userName.equals("new")){
				newUser();
				return 1;
			}
			else{
				if(useUserNameLogin() == 0){
					return 0;
				}
			}
		}
	}	
	
	private byte[] getDataByte(){
		byte[] encryptContent;
		String profile = new String(userName+"\n"+name+"\n"+Integer.toString(credit)+"\n"+Integer.toString(usrType)+"\n"+new String(home.getTree())+"\n");
		byte[] profileByte = profile.getBytes();
		encryptContent = new byte[hashByte.length+profileByte.length];
		System.arraycopy(hashByte, 0, encryptContent, 0, hashByte.length);
		System.arraycopy(profileByte, 0, encryptContent, hashByte.length, profileByte.length);
		return encryptContent;
	}
	
	private void saveUserSetting(){
		byte[] encrypted,encryptContent;
		try{
			if(!userName.equals("guset")){
				encryptContent = getDataByte();
				encrypted = encrypt(encryptContent, hashByte);
				writeOldUser(encrypted);
			}
		}
		catch (Exception e){
			System.out.println("Save error\n");
		}
	}
	
	private  void newUser()throws Exception{
		char[] passChar,passTmpChar;
		String passTmp, pass;
		String creditString = "";
		byte[] encryptContent, encrypted;
	 	while(true){
			System.out.print("Please register a username: ");
			userName = new String(scanner.nextLine());
			if(!checkUserNameUsed()){
				passTmpChar = console.readPassword("Please enter your password: ");
				passChar = console.readPassword("Please enter your password again: ");
				passTmp = new String(passTmpChar);
				pass = new String(passChar);
				if(!pass.equals(passTmp)){
					System.out.println("Password not equal!");
					continue;
				}
				hashByte = computeHash(pass);
				setName();
				usrType = 1;
				credit = 0;
				System.out.println("Thank you for signing-up!");
				encryptContent = getDataByte();
				encrypted = encrypt(encryptContent, hashByte);
				writeNewUser(encrypted);
				break;
			}
			else{
				System.out.println("Username Already been Used! Please try another one!");
			}
		}
	}
	
	private  int useUserNameLogin(){
		try{
			char[] passChar;
			byte[] buf, passTmp, decrypted, tmp;
			String pass, profile, inst;
			int i, j;
			passChar = console.readPassword("Please enter your password: ");
			pass = new String(passChar);
			hashByte = computeHash(pass);
			if(!checkUserNameUsed()){
				System.out.println("Username or password incorrect!");
				return -1;
			}
			else{
				buf = getBytesFromFile();
				decrypted = decrypt(buf, hashByte);
				passTmp = new byte[hashByte.length];
				for(i = 0;i<hashByte.length;i++){
					if(decrypted[i] != hashByte[i]) return -1;
				}
				tmp = new byte[decrypted.length-hashByte.length];
				System.arraycopy(decrypted, i, tmp, 0, tmp.length);
				profile = new String(tmp);
				StringTokenizer Tok = new StringTokenizer(profile, "\n");
				userName = (String)Tok.nextElement();
				name = (String)Tok.nextElement();
				credit = Integer.valueOf((String)Tok.nextElement());
				usrType = Integer.valueOf((String)Tok.nextElement());
				System.out.println(profile);
				while (Tok.hasMoreTokens()){
					inst = new String((String)Tok.nextElement());
					System.out.println(inst);
					dealInst(inst);
                }
				return 0;
			}
		}
		catch(Exception e){
			System.out.println(e);
			return -1;
		}
	}	
	private boolean checkUserNameUsed(){
		try{
			String fileName;
			fileName = new String(userName);
			File f = new File(fileName+".profile");
			if(f.exists())	return true;
			else return false;
		}
		catch (Exception e){
			return false;
		}
	}
	
	private void writeNewUser(byte[] encrypted){
		FileOutputStream writer = null;
		File f;
		try{
			String fileName;
			fileName = new String(userName);
			f = new File(fileName+".profile");
			f.createNewFile();
			writer = new FileOutputStream(f);
			writer.write(encrypted);
		}
		catch (Exception e){
			System.out.println(e);
		}
	}
	
	private void writeOldUser(byte[] encrypted){
		FileOutputStream writer = null;
		File f;
		try{
			String fileName;
			fileName = new String(userName);
			f = new File(fileName+".profile");
			if(f.exists()){
				writer = new FileOutputStream(f);
				writer.write(encrypted);
			}
		}
		catch (Exception e){
			System.out.println("oh no!2");
		}
	}
	
	private byte[] getBytesFromFile(){
		try{
			File file;
			String fileName;
			fileName = new String(userName);
			file = new File(fileName+".profile");
			InputStream is = new FileInputStream(file);
			long length = file.length();
			byte[] bytes = new byte[(int)length];
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length
				   && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
				offset += numRead;
			}
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file "+file.getName());
			}
			is.close();
			return bytes;
		}
		catch(Exception e){return null;}
    }		
	
	private byte[] encrypt(byte[] message, byte[] Key) throws Exception{
		SecretKeySpec key = new SecretKeySpec(Key, "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "SunJCE");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(message);
	} 
	 
	private byte[] decrypt(byte[] message, byte[] Key) throws Exception{
		SecretKeySpec key = new SecretKeySpec(Key, "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "SunJCE");
		cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(message);
	}
	private byte[] computeHash(String x) throws Exception {
		java.security.MessageDigest d = null;
		d = java.security.MessageDigest.getInstance("MD5");
		d.reset();
		d.update(x.getBytes());
		return  d.digest();
	}
}