import java.util.*;
public class POODirectory{
	private String name;
	private LinkedList<DirObject> dirObjects;
	private static final int objectLim = 1024;
	private Scanner scanner;
	
	private class DirObject{
		public POODirectory directory;
		public POOBoard board;
		public String line;
		
		public DirObject(POOBoard newBoard){
			board = new POOBoard("");
			board = newBoard;
			directory = null;
			line = null;
		}
		
		public DirObject(POODirectory newDirectory){
			directory = new POODirectory("");
			directory = newDirectory;
			board = null;
			line = null;
		}
		
		public DirObject(){
			line = new String("-----------------------------------------------");
 			directory = null;
			board = null;
		}
		
		public String show(){
			if(directory != null) return directory.getName();
			else if(board!= null) return board.getName();
			else{
				String lineTmp = new String(line);
				return lineTmp;
			}
		}
		
		public int getType(){
			if(directory != null) return 0;
			else if(board!= null) return 1;
			else{
				 return -1;
			}
		}
		
		public POODirectory getDir(){
			return directory;
		}
	
		public POOBoard getBrd(){
			return board;
		}
		
	}
	
	public POODirectory(String init_name){
		name = init_name;
		dirObjects = new LinkedList<DirObject>();
	}
	
	public String getTree(){
		int i;
		String tree = new String("");
		for(i = 0;i< dirObjects.size();i++){
			if(dirObjects.get(i).directory != null){
				tree = new String(tree+"add -d "+dirObjects.get(i).show()+"\n"+"cd "+i+"\n"+new String(dirObjects.get(i).getDir().getTree())+"cd ..\n");
			}
			else if(dirObjects.get(i).board != null){
				tree = new String(tree+"add -b "+dirObjects.get(i).show()+"\n");
			}
			else{
				tree = new String(tree+"add -l\n");
			}
		}
		return tree;
	}
	
	public void add(POOBoard newBoard){
		DirObject newObject;
		if(dirObjects.size() >= objectLim){
			System.out.println("Objects in this directory has reached it's limitaion!");
			return ;
		}
		newObject = new DirObject(newBoard);
		dirObjects.addLast(newObject);
	}
	
	
	public void add(POODirectory newDirectory){
		DirObject newObject;
		if(dirObjects.size() >= objectLim){
			System.out.println("Objects in this directory has reached it's limitaion!");
			return ;
		}
		newObject = new DirObject(newDirectory);
		dirObjects.addLast(newObject);
	}
	
	public void add_split(){
		DirObject newObject;
		if(dirObjects.size() >= objectLim){
			System.out.println("Objects in this directory has reached it's limitaion!");
			return ;
		}
		newObject = new DirObject();
		dirObjects.addLast(newObject);
	}
	
	public void del(int pos){
		if(dirObjects.get(pos) == null){
			System.out.println("Position error!");
			return ;
		}
		dirObjects.remove(pos);
	}
	
	public void move(int src, int dest){
		DirObject objectTmp = new DirObject();
		if(dirObjects.get(src)==null){
			System.out.println("Position error!");
			return ;
		}			
		try{
			if(src == dest) return ;
			else{
				objectTmp = dirObjects.get(src);
				dirObjects.remove(src);
				dirObjects.add(dest, objectTmp);
			}
		}
		catch(Exception e){
			System.out.println("Position error!");
		}
	}
	
	public void show(){
		int i;
		for(i = 0;i<dirObjects.size();i++){
			System.out.println(dirObjects.get(i).show());
		}
	}
		
	public String getName(){
		String nameTmp = new String(name);
		return nameTmp;
	}
	
	public int getObjectType(int dest){
		return dirObjects.get(dest).getType();
	}
	
	public POODirectory getDir(int dest){
		return dirObjects.get(dest).getDir();
	}
	
	public POOBoard getBrd(int dest){
		return dirObjects.get(dest).getBrd();
	}
	
	public void working(String inst){
		scanner = new Scanner(System.in);
		String nameInput, inst1, inst2, inst3;
		int src, dest;
		POOBoard newBoard;
		POODirectory newDir;
		inst1 = new String("");
		inst2 = new String("");
		inst3 = new String("");
		nameInput = new String("");
		StringTokenizer Tok = new StringTokenizer(inst);
		inst1 = (String)Tok.nextElement();
		if(inst1.equals("add")){
			try{
				inst2 = (String)Tok.nextElement();
				if(inst2.equals("-b")){
					nameInput = (String)Tok.nextElement();
					newBoard  = new POOBoard(nameInput);
					this.add(newBoard);
				}
				else if(inst2.equals("-d")){
					nameInput = (String)Tok.nextElement();
					newDir  = new POODirectory(nameInput);
					this.add(newDir);
				}
				else if(inst2.equals("-l")){
					this.add_split();
				}
				else{
					System.out.println("No such instruction:( Please use \"help\" to get some help.");
				}
			}
			catch(Exception e){
				System.out.println("No such instruction:( Please use \"help\" to get some help.");
			}
		}
		else if(inst1.equals("show")){
			this.show();
		}
		else if(inst1.equals("del")){
			try{
				inst2 = (String)Tok.nextElement();
				this.del(Integer.valueOf(inst2));
			}
			catch (Exception e){
				System.out.println("Position error!");
			}
		}	
		else if(inst1.equals("move")){
			try{
				inst2 = (String)Tok.nextElement();
				src = Integer.valueOf(inst2);
				inst3 = (String)Tok.nextElement();
				dest = Integer.valueOf(inst3);
				this.move(src, dest);
			}
			catch (Exception e){
				System.out.println("Position error!");
			}
		}
		else{
			System.out.println("No such instruction:( Please use \"help\" to get some help.");
		}
	}
}