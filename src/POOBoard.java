import java.util.*;
public class POOBoard{
	private String name;
	private LinkedList<POOArticle> articles;
	private int articleTotal;
	private static final int articleLim = 1024;
	private int[] ID;
	
	public POOBoard(String init_Name){
		int i;
		name = init_Name;
		articles = new LinkedList<POOArticle>();
		articleTotal = 0;
		ID = new int[1000];
		for(i = 0;i<1000;i++) ID[i] = 0;
	}
	
	public int getID(){
		int i, id = 0;
		for(i = 0;i<1000;i++){
			if(ID[i] == 0){
				id = i;
				break;
			}
		}
		if(id == 1000) return -1;
		ID[id] = 1;
		return id;
	}
	
	public int[] idToArr(int id){
		int[] idArr = new int[3];
		idArr[2] = id%10;
		id-=idArr[2];
		idArr[1] = id%100;
		id-=idArr[1];
		idArr[0] = id%1000;
		return idArr;
	}
	
	public POOArticle artCreate(String author){
		if(articleTotal >= articleLim){
			System.out.println("Article number has reached it's limitaion!");
			return null;
		}
		int id;
		id = getID();
		POOArticle articleTmp = new POOArticle(idToArr(id), author);
		return articleTmp;
	}
	
	public void add(POOArticle article){
		if(article == null) return ;
		articles.addLast(article);
		articleTotal++;
	}
	
	public void del(int pos){
		if(articles.get(pos)==null){
			System.out.println("Position error!");
			return ;
		}			
		articles.remove(pos);
		articleTotal--;
	}
	
	public void move(int src, int dest){
		int[] tmp = new int[3];
		tmp[0] = 0;
		tmp[1] = 0;
		tmp[2] = 0;
		POOArticle articleTmp = new POOArticle(tmp, "", "", "");
		if(articles.get(src)==null){
			System.out.println("Position error!");
			return ;
		}			
		try{
			if(src == dest) return ;
			else if(src > dest){
				articleTmp = articles.get(src);
				articles.remove(src);
				articles.add(dest, articleTmp);
			}
			else if(dest > src){
				articleTmp = articles.get(src);
				articles.add(dest, articleTmp);
				articles.remove(src);
			}
			else ;
		}
		catch(Exception e){
			System.out.println("Position error!");
		}
	}
	
	public int length(){
		int length = articleTotal;
		return length;
	}
	
	public void show(){
		int i;
		for(i = 0;i<articleTotal;i++){
			System.out.println(articles.get(i).getTitle());
		}
	}
	
	public String getName(){
		String nameTmp = new String(name);
		return nameTmp;
	}
	
	public POOArticle getArt(int dest){
		return articles.get(dest);
	}
		
	
	public void working(String inst){
		Scanner scanner;
		scanner = new Scanner(System.in);
		String nameInput,inst1, inst2, inst3;
		int src, dest;
		POOBoard newBoard;
		POODirectory newDir;
		inst1 = new String("");
		inst2 = new String("");
		inst3 = new String("");
		StringTokenizer Tok = new StringTokenizer(inst);
		inst1 = (String)Tok.nextElement();
		if(inst1.equals("add")){
			inst2 = (String)Tok.nextElement();
			add(artCreate(inst2));
		}
		else if(inst1.equals("show")){
			this.show();
		}
		else if(inst1.equals("del")){
			try{
				inst2 = (String)Tok.nextElement();
				this.del(Integer.valueOf(inst2));
			}
			catch (Exception e){
				System.out.println("Position error!");
			}
		}	
		else if(inst1.equals("move")){
			try{
				inst2 = (String)Tok.nextElement();
				src = Integer.valueOf(inst2);
				inst3 = (String)Tok.nextElement();
				dest = Integer.valueOf(inst3);
				this.move(src, dest);
			}
			catch (Exception e){
				System.out.println("Position error!");
			}
		}
		else{
			System.out.println("No such instruction:( Please use \"help\" to get some help.");
		}
	}
}