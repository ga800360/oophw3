import java.util.*;
public class POOArticle{
	private int[] ID;
	private String title;
	private String author;
	private String content;
	private int evalCount;
	private int evalTotal;
	private String[] evalMes;
	private Scanner scanner;
	private static final int MAXEVAL = 1024;
	
	public POOArticle(int[] init_ID, String init_title, String init_author, String init_content){
		scanner = new Scanner(System.in);
		ID = new int[3];
		title = new String(init_title);
		author = new String(init_author);
		content = new String(init_content);
		evalMes = new String[MAXEVAL];
		ID = init_ID;
		evalCount = 0;
		evalTotal = 0;
	}
	
	public POOArticle(int[] init_ID, String init_author){
		scanner = new Scanner(System.in);
		ID = new int[3];
		evalMes = new String[MAXEVAL];
		author = init_author;
		System.out.print("Please input title:");
		title = scanner.nextLine();
		System.out.print("Please input content:");
		content = scanner.nextLine();
		ID = init_ID;
		evalCount = 0;
		evalTotal = 0;
	}
	
	public void push(String userName, String eval){
		if(evalCount >= MAXEVAL){
			System.out.println("Evaluation number has reached it's limitaion!");
			return ;
		}
		evalCount++;
		addEval(userName," push: "+eval);
	}
	
	public void boo(String userName, String eval){
		if(evalCount >= MAXEVAL){
			System.out.println("Evaluation number has reached it's limitaion!");
			return ;
		}
		evalCount--;
		addEval(userName," boo:  "+eval);
	}
	
	public void arrow(String userName, String eval){
		if(evalCount >= MAXEVAL){
			System.out.println("Evaluation number has reached it's limitaion!");
			return ;
		}
		addEval(userName," -->:  "+eval);
	}
	
	public void show(){
		int i;
		System.out.println(content);
		for(i = 0;i<evalTotal;i++){
			System.out.println(evalMes[i]);
		}
	}
	
	public void list(){
		System.out.print(evalCount+" "+ID[0]+""+ID[1]+""+ID[2]+""+" "+title+" "+author+"\n");
	}
	
	public String getTitle(){
		String titleTmp = new String(title);
		return titleTmp;
	}
	
	private void addEval(String userName, String eval){
		evalMes[evalTotal] = new String(userName + " " + eval);
		evalTotal++;
	}
	
	public void working(String inst){
		try{
			String nameInput,inst1, userName, eval;
			int src, dest; 
			POOBoard newBoard;
			POODirectory newDir;
			StringTokenizer Tok = new StringTokenizer(inst);
			inst1 = new String((String)Tok.nextElement());
			if(inst1.equals("push")){
				eval = new String((String)Tok.nextElement());
				userName = new String((String)Tok.nextElement());
				push(userName, eval);
			}
			else if(inst1.equals("boo")){
				eval = new String((String)Tok.nextElement());
				userName = new String((String)Tok.nextElement());
				boo(userName,eval);
			}
			else if(inst1.equals("arrow")){
				eval = new String((String)Tok.nextElement());
				userName = new String((String)Tok.nextElement());
				arrow(userName, eval);
			}
			else  if(inst1.equals("show")){
				show();
			}
			else if(inst1.equals("list")){
				list();
			}
			else{
				System.out.println("No such instruction:( Please use \"help\" to get some help.");
			}
		}
		catch(Exception e){
			System.out.println("No such instruction:( Please use \"help\" to get some help.");
		}
	}
}